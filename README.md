##Config Params for Auto Scaling Group
* Nodejs 4.3
* Timeout 5 sec
* Lambda must be part of VPC and security groups 

Insert AutoScaling GroupNames in array Support for multiple ASG:  

  
        var params_asg = {
	  AutoScalingGroupNames: [
	    'my-ASG',
	    /* more items */
	  ]
	};

trigger URL for you service :
example http://192.168.0.1/test/service


		var url = "/test/service";




##IAM Role for Lambda

		
	{
    		"Version": "2012-10-17",
    		"Statement": [
        		{
            	"Effect": "Allow",
            	"Action": [
                	   "autoscaling:*",
                	   "ec2:*",
                	   "ec2:CreateNetworkInterface",
                	   "ec2:DescribeNetworkInterfaces",
                           "ec2:DeleteNetworkInterface"
                          ],
                 "Resource": "*"
        		}
    		]
	}